﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Libraries.Bets.Data.Tests
{
    [TestClass]
    public class ReadTest
    {
        [TestMethod]
        public void ReadSettledTrades()
        {
            var repository = new SettledTradeRepository();
            Assert.AreEqual(repository.Count, 50);
        }

        [TestMethod]
        public void ReadUnsettledTrades()
        {
            var repository = new UnsettledTradeRepository();
            Assert.AreEqual(repository.Count, 22);
        }
    }
}
