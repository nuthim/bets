﻿namespace BetInspector
{
    partial class Blotter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.settledControl1 = new BetInspector.SettledControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.unsettledControl1 = new BetInspector.UnsettledControl();
            this.tab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab
            // 
            this.tab.Controls.Add(this.tabPage1);
            this.tab.Controls.Add(this.tabPage2);
            this.tab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tab.Location = new System.Drawing.Point(0, 0);
            this.tab.Name = "tab";
            this.tab.SelectedIndex = 0;
            this.tab.Size = new System.Drawing.Size(923, 523);
            this.tab.TabIndex = 0;
            this.tab.Selected += new System.Windows.Forms.TabControlEventHandler(this.OnTabSelected);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.settledControl1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(915, 497);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Settled Trades";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // settledControl1
            // 
            this.settledControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settledControl1.Location = new System.Drawing.Point(3, 3);
            this.settledControl1.Name = "settledControl1";
            this.settledControl1.RiskManager = null;
            this.settledControl1.Size = new System.Drawing.Size(909, 491);
            this.settledControl1.TabIndex = 0;
            this.settledControl1.TradeManager = null;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.unsettledControl1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(915, 497);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Unsettled Trades";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // unsettledControl1
            // 
            this.unsettledControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.unsettledControl1.Location = new System.Drawing.Point(3, 3);
            this.unsettledControl1.Name = "unsettledControl1";
            this.unsettledControl1.RiskManager = null;
            this.unsettledControl1.Size = new System.Drawing.Size(909, 491);
            this.unsettledControl1.TabIndex = 0;
            this.unsettledControl1.TradeManager = null;
            // 
            // Blotter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 523);
            this.Controls.Add(this.tab);
            this.Name = "Blotter";
            this.Text = "Trade Blotter";
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.tab.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tab;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private SettledControl settledControl1;
        private UnsettledControl unsettledControl1;
    }
}

