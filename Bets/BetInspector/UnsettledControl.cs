﻿using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using Libraries.Bets.Business;
using Libraries.Bets.Business.Definitions;
using Libraries.Bets.Business.Calculators;

namespace BetInspector
{
    public partial class UnsettledControl : UserControlBase
    {
        public UnsettledControl()
        {
            InitializeComponent();
        }

        public override void Display()
        {
            foreach (var risk in RiskManager.GetUnsettledRisks())
            {
                var calculator = RiskCalculatorFactory.GetRiskCalculator(TradeManager.GetSettledTrades(), TradeManager.GetUnsettledTrades(), risk);
                calculator.Calculate(risk);
            }

            grid.DataSource = TradeManager.GetUnsettledTrades();
        }

        
    }
}
