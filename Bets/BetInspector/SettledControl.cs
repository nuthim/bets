﻿using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using Libraries.Bets.Business;
using Libraries.Bets.Business.Definitions;
using Libraries.Bets.Business.Calculators;

namespace BetInspector
{
    public partial class SettledControl : UserControlBase
    {
        public SettledControl()
        {
            InitializeComponent();
        }

        public override void Display()
        {
            foreach (var risk in RiskManager.GetSettledRisks())
            {
                var calculator = RiskCalculatorFactory.GetRiskCalculator(TradeManager.GetSettledTrades(), TradeManager.GetUnsettledTrades(), risk);
                calculator.Calculate(risk);
            }

            grid.DataSource = TradeManager.GetSettledTrades();
        }

        
    }
}
