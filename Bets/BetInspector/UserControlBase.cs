﻿using Libraries.Bets.Business;
using System.Windows.Forms;

namespace BetInspector
{
    public class UserControlBase : UserControl
    {
        public IRiskManager RiskManager
        {
            get; set;
        }

        public ITradeManager TradeManager
        {
            get; set;
        }

        public virtual void Display()
        {

        }
    }
}
