﻿using System;
using System.Linq;
using System.Windows.Forms;
using Libraries.Bets.Business;


namespace BetInspector
{
    public partial class Blotter : Form
    {
        private ITradeManager _tradeManager;
        private IRiskManager _riskManager;
        public Blotter()
        {
            InitializeComponent();
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            foreach (TabPage tab in tab.TabPages)
            {
                tab.Controls.OfType<UserControlBase>().ToList().ForEach(x =>
                {
                    x.RiskManager = RiskManager;
                    x.TradeManager = TradeManager;
                });
            }

            tab.SelectedIndex = 0;
            tab.SelectedTab.Controls.OfType<UserControlBase>().First().Display();
        }

        public IRiskManager RiskManager
        {
            get
            {
                if (_riskManager == null)
                    _riskManager = new RiskManager();

                return _riskManager;
            }
        }

        public ITradeManager TradeManager
        {
            get
            {
                if (_tradeManager == null)
                    _tradeManager = new TradeManager();

                return _tradeManager;
            }
        }

        private void OnTabSelected(object sender, TabControlEventArgs e)
        {
            e.TabPage.Controls.OfType<UserControlBase>().First().Display();
        }
    }
}
