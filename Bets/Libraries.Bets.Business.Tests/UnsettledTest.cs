﻿using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Libraries.Bets.Data.Entities;
using Libraries.Bets.Business.Calculators;
using Libraries.Bets.Business.Definitions;

namespace Libraries.Bets.Business.Tests
{
    [TestClass]
    public class UnsettledTest
    {
        [TestMethod]
        public void UnusualBetTest1()
        {
            var trades = new List<SettledTradeModel>();
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Stake = 10 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Stake = 20 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Stake = 30 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Stake = 40 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Stake = 10 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Stake = 10 }));

            var untrades = new List<UnsettledTradeModel>();
            untrades.Add(new UnsettledTradeModel(new UnsettledTrade { CustomerId = 1, Stake = 100 }));
            untrades.Add(new UnsettledTradeModel(new UnsettledTrade { CustomerId = 1, Stake = 20 }));
            untrades.Add(new UnsettledTradeModel(new UnsettledTrade { CustomerId = 1, Stake = 30 }));

            var average = trades.Average(x=> x.Stake);

            var calculator = new UnusualBetRiskCalculator(trades, untrades);
            calculator.Calculate(new UnusualBetDefinition { Excess = 20, Classification = RiskCategory.Extreme });

            Assert.IsTrue(untrades[0].Risk == RiskCategory.Extreme);
        }

        [TestMethod]
        public void UnusualBetTest2()
        {
            var trades = new List<SettledTradeModel>();
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Stake = 10 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Stake = 20 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Stake = 30 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Stake = 40 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Stake = 10 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Stake = 10 }));

            var untrades = new List<UnsettledTradeModel>();
            untrades.Add(new UnsettledTradeModel(new UnsettledTrade { CustomerId = 1, Stake = 100 }));
            untrades.Add(new UnsettledTradeModel(new UnsettledTrade { CustomerId = 1, Stake = 20 }));
            untrades.Add(new UnsettledTradeModel(new UnsettledTrade { CustomerId = 1, Stake = 30 }));

            var average = trades.Average(x => x.Stake);

            var calculator = new UnusualBetRiskCalculator(trades, untrades);
            calculator.Calculate(new UnusualBetDefinition { Excess = 20, Classification = RiskCategory.Extreme });

            Assert.IsTrue(untrades[1].Risk == RiskCategory.None);
        }

        [TestMethod]
        public void UnusualBetTest3()
        {
            var trades = new List<SettledTradeModel>();
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Stake = 10 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Stake = 20 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Stake = 30 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Stake = 40 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Stake = 10 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Stake = 10 }));

            var untrades = new List<UnsettledTradeModel>();
            untrades.Add(new UnsettledTradeModel(new UnsettledTrade { CustomerId = 1, Stake = 100 }));
            untrades.Add(new UnsettledTradeModel(new UnsettledTrade { CustomerId = 1, Stake = 20 }));
            untrades.Add(new UnsettledTradeModel(new UnsettledTrade { CustomerId = 1, Stake = 30 }));

            var average = trades.Average(x => x.Stake);

            var calculator = new UnusualBetRiskCalculator(trades, untrades);
            calculator.Calculate(new UnusualBetDefinition { Excess = 20, Classification = RiskCategory.Extreme });

            Assert.IsTrue(untrades[2].Risk == RiskCategory.Extreme);
        }
    }
}
