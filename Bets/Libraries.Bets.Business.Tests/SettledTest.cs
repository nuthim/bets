﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Libraries.Bets.Business.Calculators;
using System.Collections.Generic;
using Libraries.Bets.Data.Entities;
using Libraries.Bets.Business.Definitions;

namespace Libraries.Bets.Business.Tests
{
    [TestClass]
    public class SettledTest
    {
        [TestMethod]
        public void TestLess()
        {
            var trades = new List<SettledTradeModel>();
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Win = 0 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Win = 0 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Win = 0 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Win = 0 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Win = 20 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Win = 20 }));

            var calculator = new UnusualWinRiskCalculator(trades);
            calculator.Calculate(new UnusualWinDefinition { WinRatio = 50, Classification = RiskCategory.Extreme });

            Assert.IsFalse(trades.Where(x => x.CustomerId == 1).All(y => y.Risk == RiskCategory.Extreme));
        }

        [TestMethod]
        public void TestExact()
        {
            var trades = new List<SettledTradeModel>();
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Win = 0 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Win = 10 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Win = 0 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Win = 0 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Win = 20 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Win = 20 }));

            var calculator = new UnusualWinRiskCalculator(trades);
            calculator.Calculate(new UnusualWinDefinition { WinRatio = 50, Classification = RiskCategory.Extreme });

            Assert.IsTrue(trades.Where(x => x.CustomerId == 1).All(y => y.Risk == RiskCategory.Extreme));
        }

        [TestMethod]
        public void TestMore()
        {
            var trades = new List<SettledTradeModel>();
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Win = 0 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Win = 10 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Win = 0 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Win = 0 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Win = 20 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Win = 20 }));
            trades.Add(new SettledTradeModel(new SettledTrade { CustomerId = 1, Win = 20 }));

            var calculator = new UnusualWinRiskCalculator(trades);
            calculator.Calculate(new UnusualWinDefinition { WinRatio = 50, Classification = RiskCategory.Extreme });

            Assert.IsTrue(trades.Where(x => x.CustomerId == 1).All(y => y.Risk == RiskCategory.Extreme));
        }
    }
}
