﻿using Libraries.Bets.Data.Entities;
using System;
using System.Collections.Generic;

namespace Libraries.Bets.Data
{
    public class RiskRepository : IRepository<Risk>
    {
        protected List<Risk> _risks = new List<Risk>();

        public int Count
        {
            get
            {
                return _risks.Count;
            }
        }

        public void Add(Risk item)
        {
            throw new NotImplementedException();
        }

        public void Delete(Risk item)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Risk> ReadAll()
        {
            //Assume we get such information from database
            _risks.Add(new Risk { Data = "{WinRatio = 60, Classification=Extreme}", Type = "Libraries.Bets.Business.Definitions.UnusualWinDefinition" });
            _risks.Add(new Risk { Data = "{Excess = 10, Classification=Considerable}", Type = "Libraries.Bets.Business.Definitions.UnusualBetDefinition" });
            _risks.Add(new Risk { Data = "{Excess = 30, Classification=Extreme}", Type = "Libraries.Bets.Business.Definitions.UnusualBetDefinition" });
            _risks.Add(new Risk { Data = "{Stake = 1000, Classification=Extreme}", Type = "Libraries.Bets.Business.Definitions.HighStakeDefinition" });
            return _risks;
        }
    }
}
