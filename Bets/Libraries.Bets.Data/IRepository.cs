﻿using System.Collections.Generic;

namespace Libraries.Bets.Data
{
    public interface IRepository<T>
    {
        int Count { get; }
        void Add(T item);
        void Delete(T item);
        IEnumerable<T> ReadAll();

    }
}
