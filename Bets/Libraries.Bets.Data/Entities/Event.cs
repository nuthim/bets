﻿using System.Collections.Generic;

namespace Libraries.Bets.Data.Entities
{
    public class Event
    {
        public int Id { get; set; }
        public ICollection<Customer> Customers { get; set; }
    }
}
