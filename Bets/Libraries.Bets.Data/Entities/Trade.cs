﻿

namespace Libraries.Bets.Data.Entities
{
    public abstract class Trade
    {
        public int CustomerId { get; set; }
        public int EventId { get; set; }
        public int ParticipantId { get; set; }
        public double Stake { get; set; }

    }
}
