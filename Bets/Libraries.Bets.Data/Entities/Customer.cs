﻿using System.Collections.Generic;

namespace Libraries.Bets.Data.Entities
{
    public class Customer
    {
        public int Id { get; set; }
        public ICollection<Event> Events { get; set; }
    }
}
