﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libraries.Bets.Data.Entities
{
    public class Risk
    {
        public string Data { get; set; }

        public string Type { get; set; }
    }
}
