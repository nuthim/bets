﻿
namespace Libraries.Bets.Data.Entities
{
    public class UnsettledTrade : Trade
    {
        public double ToWin { get; set; }
    }
}
