﻿
namespace Libraries.Bets.Data.Entities
{
    public class SettledTrade : Trade
    {
        public double Win { get; set; }
    }
}
