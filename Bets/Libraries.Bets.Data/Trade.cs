﻿

namespace Libraries.Bets.Data
{
    public abstract class Trade
    {
        public int Customer { get; set; }
        public int Event { get; set; }
        public int Participant { get; set; }
        public int Stake { get; set; }

    }
}
