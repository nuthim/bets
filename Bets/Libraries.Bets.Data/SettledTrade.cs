﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libraries.Bets.Data
{
    public class SettledTrade : Trade
    {
        public int Win { get; set; }
    }
}
