﻿using System.IO;
using System.Linq;
using System.Collections.Generic;
using Libraries.Bets.Data.Entities;

namespace Libraries.Bets.Data
{
    public class SettledTradeRepository : TradeRepository<SettledTrade>
   {
        public override IEnumerable<SettledTrade> ReadAll()
        {
            if (_trades != null)
                return _trades;

            var contents = File.ReadAllText(@".\Data\Settled.csv").Split('\n');
            var csv = from line in contents
                      select line.Split(',');

            _trades = csv.Skip(1).Where(x => x.Length == 5).Select(x =>
            (
               new SettledTrade
               {
                   CustomerId = int.Parse(x[0]),
                   EventId = int.Parse(x[1]),
                   ParticipantId = int.Parse(x[2]),
                   Stake = int.Parse(x[3]),
                   Win = int.Parse(x[4])
               }
            )).ToList();

            return _trades;
        }
    }
}
