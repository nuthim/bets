﻿using System.Linq;
using System.Collections.Generic;
using Libraries.Bets.Data.Entities;


namespace Libraries.Bets.Data
{
    public abstract class TradeRepository<T> : IRepository<T> where T : Trade
    {
        protected List<T> _trades;

        public int Count
        {
            get
            {
                return ReadAll().Count();
            }
        }
        public abstract IEnumerable<T> ReadAll();
        public void Add(T item)
        {
            if (_trades == null)
                _trades = new List<T>();

            _trades.Add(item);
        }

        public void Delete(T item)
        {
            if (_trades == null)
                return;

            _trades.Remove(item);
        }
    }
}
