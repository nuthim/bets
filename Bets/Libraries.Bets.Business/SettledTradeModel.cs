﻿using Libraries.Bets.Data.Entities;


namespace Libraries.Bets.Business
{
    public class SettledTradeModel : TradeModel<SettledTrade>
    {
        public SettledTradeModel(SettledTrade trade) : base(trade)
        {

        }
        public double Win
        {
            get { return Trade.Win; }
            set
            {
                Trade.Win = value;
                NotifyPropertyChanged("Win");
            }
        }
    }
}
