﻿using System.ComponentModel;
using Libraries.Bets.Data.Entities;

namespace Libraries.Bets.Business
{
    public abstract class TradeModel<T> : INotifyPropertyChanged where T : Trade
    {
        private T _trade;
        private RiskCategory _risk;
        protected TradeModel(T trade)
        {
            _trade = trade;
        }

        protected T Trade
        {
            get { return _trade; }
        }
        public int CustomerId
        {
            get { return _trade.CustomerId; }
            set
            {
                _trade.CustomerId = value;
                NotifyPropertyChanged("CustomerId");
            }
        }
        public int EventId
        {
            get { return _trade.EventId; }
            set
            {
                _trade.EventId = value;
                NotifyPropertyChanged("EventId");
            }
        }
        public int ParticipantId
        {
            get { return _trade.ParticipantId; }
            set
            {
                _trade.ParticipantId = value;
                NotifyPropertyChanged("ParticipantId");
            }
        }
        public double Stake
        {
            get { return _trade.Stake; }
            set
            {
                _trade.Stake = value;
                NotifyPropertyChanged("Stake");
            }
        }
        public RiskCategory Risk
        {
            get { return _risk; }
            set
            {
                _risk = value;
                NotifyPropertyChanged("Risk");
            }
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
