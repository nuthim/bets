﻿using System.Linq;
using Libraries.Bets.Business.Definitions;
using Libraries.Bets.Data;
using Libraries.Bets.Data.Entities;
using System.Collections.Generic;
using System;

namespace Libraries.Bets.Business
{
    public interface IRiskManager
    {
        IEnumerable<IRiskDefinition> GetSettledRisks();

        IEnumerable<IRiskDefinition> GetUnsettledRisks();

    }

    public class RiskManager : IRiskManager
    {
        public RiskManager()
        {

        }

        public IEnumerable<IRiskDefinition> GetSettledRisks()
        {
            var list = new List<IRiskDefinition>();

            //Assume we read the risk repository and return a list of risk definitions
            list.Add(new UnusualWinDefinition { WinRatio = 60, Classification = RiskCategory.Extreme });

            return list;
        }

        public IEnumerable<IRiskDefinition> GetUnsettledRisks()
        {
            var list = new List<IRiskDefinition>();

            list.Add(new UnusualBetDefinition { Excess = 10, Classification = RiskCategory.Considerable });
            list.Add(new UnusualBetDefinition { Excess = 30, Classification = RiskCategory.Extreme });
            list.Add(new HighStakeDefinition { Value = 1000, Classification = RiskCategory.Extreme });

            return list;
        }


    }
}
