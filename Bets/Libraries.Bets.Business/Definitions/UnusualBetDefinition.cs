﻿

namespace Libraries.Bets.Business.Definitions
{
    public class UnusualBetDefinition : IRiskDefinition
    {
        public double Excess
        {
            get; set;
        }

        public RiskCategory Classification
        {
            get; set;
        }
    }
}
