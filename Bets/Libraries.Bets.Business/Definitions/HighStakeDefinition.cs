﻿

namespace Libraries.Bets.Business.Definitions
{
    public class HighStakeDefinition : IRiskDefinition
    {
        public double Value
        {
            get; set;
        }
        public RiskCategory Classification
        {
            get; set;
        }
    }
}
