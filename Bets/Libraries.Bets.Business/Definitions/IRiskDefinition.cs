﻿
namespace Libraries.Bets.Business.Definitions
{
    public interface IRiskDefinition
    {
        RiskCategory Classification { get; set; }
    }

}
