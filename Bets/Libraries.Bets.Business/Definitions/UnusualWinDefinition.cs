﻿
namespace Libraries.Bets.Business.Definitions
{
    public class UnusualWinDefinition : IRiskDefinition
    {
        public int WinRatio
        {
            get; set;
        }

        public RiskCategory Classification
        {
            get; set;
        }
    }
}
