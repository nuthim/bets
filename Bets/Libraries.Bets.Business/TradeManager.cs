﻿using Libraries.Bets.Data;
using Libraries.Bets.Data.Entities;
using System.Collections.Generic;

namespace Libraries.Bets.Business
{
    public interface ITradeManager
    {
        IEnumerable<SettledTradeModel> GetSettledTrades();
        IEnumerable<UnsettledTradeModel> GetUnsettledTrades();
    }

    public class TradeManager : ITradeManager
    {
        private IRepository<SettledTrade> _settledRepo;
        private IRepository<UnsettledTrade> _unsettledRepo;
        private IEnumerable<SettledTradeModel> _settledTrades;
        private IEnumerable<UnsettledTradeModel> _unsettledTrades;
        
        public TradeManager()
        {
            _settledRepo = new SettledTradeRepository();
            _unsettledRepo = new UnsettledTradeRepository();
        }

        public IEnumerable<SettledTradeModel> GetSettledTrades()
        {
            if (_settledTrades == null)
            {
                var list = new List<SettledTradeModel>();
                foreach (var item in _settledRepo.ReadAll())
                    list.Add(new SettledTradeModel(item));

                _settledTrades = list;
            }

            return _settledTrades;
        }

        public IEnumerable<UnsettledTradeModel> GetUnsettledTrades()
        {
            if (_unsettledTrades == null)
            {
                var list = new List<UnsettledTradeModel>();
                foreach (var item in _unsettledRepo.ReadAll())
                    list.Add(new UnsettledTradeModel(item));

                _unsettledTrades = list;
            }

            return _unsettledTrades;
        }
    }
}
