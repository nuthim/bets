﻿using Libraries.Bets.Data.Entities;


namespace Libraries.Bets.Business
{
    public class UnsettledTradeModel : TradeModel<UnsettledTrade>
    {
        public UnsettledTradeModel(UnsettledTrade trade) : base(trade)
        {

        }
        public double ToWin
        {
            get { return Trade.ToWin; }
            set
            {
                Trade.ToWin = value;
                NotifyPropertyChanged("ToWin");
            }
        }
    }
}
