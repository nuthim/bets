﻿

namespace Libraries.Bets.Business
{
    public enum RiskCategory
    {
        None,
        Considerable,
        Extreme
    }
}
