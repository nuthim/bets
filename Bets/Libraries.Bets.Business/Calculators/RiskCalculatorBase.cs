﻿using Libraries.Bets.Business.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libraries.Bets.Business.Calculators
{
    public abstract class RiskCalculatorBase : IRiskCalculator
    {
        public abstract void Calculate(IRiskDefinition definition);
    }
}
