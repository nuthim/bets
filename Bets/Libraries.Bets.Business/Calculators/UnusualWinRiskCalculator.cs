﻿using Libraries.Bets.Business.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Libraries.Bets.Business.Calculators
{
    public class UnusualWinRiskCalculator : RiskCalculatorBase
    {
        private IEnumerable<SettledTradeModel> _settledTrades;

        public UnusualWinRiskCalculator(IEnumerable<SettledTradeModel> settledTrades)
        {
            _settledTrades = settledTrades;
        }

        public override void Calculate(IRiskDefinition definition)
        {
            Calculate(definition as UnusualWinDefinition);
        }

        public void Calculate(UnusualWinDefinition definition)
        {
            if (definition == null)
                throw new ArgumentNullException("definition");

            foreach (var key in _settledTrades.GroupBy(x => x.CustomerId))
            {
                double wins = key.Where(y => y.Win != 0).Count();
                if (wins / key.Count() >= definition.WinRatio / 100d)
                    key.Where(c=> c.Risk < definition.Classification).ToList().ForEach(c => c.Risk = definition.Classification);
            }
        }
    }
}
