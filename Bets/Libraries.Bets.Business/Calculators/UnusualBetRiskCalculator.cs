﻿using Libraries.Bets.Business.Definitions;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Libraries.Bets.Business.Calculators
{
    public class UnusualBetRiskCalculator : RiskCalculatorBase
    {
        private IEnumerable<SettledTradeModel> _settledTrades;
        private IEnumerable<UnsettledTradeModel> _unsettledTrades;

        public UnusualBetRiskCalculator(IEnumerable<SettledTradeModel> settledTrades, 
            IEnumerable<UnsettledTradeModel> unsettledTrades)
        {
            _settledTrades = settledTrades;
            _unsettledTrades = unsettledTrades;
        }

        public override void Calculate(IRiskDefinition definition)
        {
            Calculate(definition as UnusualBetDefinition);
        }

        public void Calculate(UnusualBetDefinition definition)
        {
            var tradeList = _unsettledTrades.Where(x => x.Risk < definition.Classification).ToArray();

            foreach (var trade in tradeList)
            {
                double average = GetCustomerAverage(trade.CustomerId);
                if (trade.Stake >= average * (1 + definition.Excess/100))
                    trade.Risk = definition.Classification;
            }
        }

        private double GetCustomerAverage(int customerId)
        {
            return _settledTrades.Where(x => x.CustomerId == customerId).Average(x => x.Stake);
        }
    }
}
