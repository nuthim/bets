﻿using Libraries.Bets.Business.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Libraries.Bets.Business.Calculators
{
    class HighValueRiskCalculator : RiskCalculatorBase
    {
        private IEnumerable<UnsettledTradeModel> _unsettledTrades;

        public HighValueRiskCalculator(IEnumerable<UnsettledTradeModel> unsettledTrades)
        {
            _unsettledTrades = unsettledTrades;
        }

        public override void Calculate(IRiskDefinition definition)
        {
            Calculate(definition as HighStakeDefinition);
        }

        public void Calculate(HighStakeDefinition definition)
        {
            if (definition == null)
                throw new ArgumentNullException("definition");

            _unsettledTrades.Where(x => x.Risk < definition.Classification && x.ToWin >= definition.Value).ToList().ForEach(y => y.Risk = definition.Classification);
        }
    }
}
