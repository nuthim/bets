﻿using Libraries.Bets.Business.Definitions;
using System.Collections.Generic;

namespace Libraries.Bets.Business.Calculators
{
    public static class RiskCalculatorFactory
    {
        public static IRiskCalculator GetRiskCalculator(
            IEnumerable<SettledTradeModel> settledTrades,
            IEnumerable<UnsettledTradeModel> unsettledTrades, 
            IRiskDefinition definition)
        {
            var riskType = definition.GetType();

            if (riskType == typeof(UnusualWinDefinition))
                return new UnusualWinRiskCalculator(settledTrades);

            if (riskType == typeof(UnusualBetDefinition))
                return new UnusualBetRiskCalculator(settledTrades, unsettledTrades);

            if (riskType == typeof(HighStakeDefinition))
                return new HighValueRiskCalculator(unsettledTrades);

            return null;
        }
    }
}
