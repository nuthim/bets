﻿using Libraries.Bets.Business.Definitions;

namespace Libraries.Bets.Business.Calculators
{
    public interface IRiskCalculator
    {
        void Calculate(IRiskDefinition definition);
    }
}
